from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.fe import *

class FE_1D(FE):
    """
    Abstract base class for finite elements in 1D.
    It implements a derivative evaluation using numerical differentiation.
    """    
    num_diff_warned = False

    def __init__(self):
        self.eltype = "segment"
        self.dim = 1

    @abstractmethod
    def _evaluate_id(self, ip):
        raise Exception("Not implemented - Base class should not be used")

    def _evaluate_deriv(self, ip):
        # numerical differentiation - should be overwritten by subclasses
        # for proper accuracy and performance
        if not FE_1D.num_diff_warned:
            print("Warning: Using numerical differentiation for deriv evaluation in " + str(type(self)) + " object.")
            FE_1D.num_diff_warned = True
        eps = 1e-8
        left = ip.copy() - eps
        right = ip.copy() + eps
        return ((self._evaluate_id(right) - self._evaluate_id(left))/(2*eps)).reshape((1,self.ndof))

class P1_Segment_FE(FE_1D, Lagrange_FE):
    """
    This class represents a P1 segment finite element.
    """
    ndof = 2
    order = 1
    def __init__(self):
        super().__init__()
        self.nodes = [ np.array([0]), np.array([1]) ]

    def _evaluate_id(self, ip):
        """
        Evaluates the P1 segment finite element at the given integration point.

        Parameters:
        ip (numpy.ndarray): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the P1 segment finite element at the given integration point.
        """
        return array([1-ip[0], ip[0]])

    def __str__(self):
        return "P1 Segment Finite Element\n" + super().__str__()
    
class P1Mod_Segment_FE(FE_1D, Lagrange_FE):
    """
    This class represents a P1 segment finite element.
    """
    ndof = 3
    order = 2
    def __init__(self):
        super().__init__()
        self.nodes = [ np.array([0]), np.array([0.5]), np.array([1])]

    def _evaluate_id(self, ip):
        """
        Evaluates the P1 segment finite element at the given integration point.

        Parameters:
        ip (numpy.ndarray): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the P1 segment finite element at the given integration point.
        """
        return array([3*ip[0]*ip[0]-4*ip[0]+1, 3*ip[0]*ip[0]-2*ip[0], -ip[0]*ip[0]+ip[0]])

    def __str__(self):
        return "P1 Segment Mod Finite Element\n" + super().__str__()

    def _evaluate_deriv(self, ip):
        return np.full(shape=ip.shape + (2,), fill_value=[-1,1])

class P2_Segment_FE(FE_1D):
    """
    This class represents a P1 segment finite element.
    """
    ndof = 3
    order = 2
    def __init__(self):
        super().__init__()
        self.nodes = [ np.array([0]), np.array([1]) ]

    def _evaluate_id(self, ip):
        """
        Evaluates the P2 segment finite element at the given integration point.

        Parameters:
        ip (numpy.ndarray): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the P2 segment finite element at the given integration point.
        """
        return array([1-ip[0], ip[0], 4*ip[0]*(1-ip[0])])

    def __str__(self):
        return "P2 Segment Finite Element\n" + super().__str__()

    def _evaluate_deriv(self, ip):
        deriv = np.zeros(ip.shape + (3,))
        deriv[:, 0] = -1
        deriv[:, 1] = 1
        deriv[:, 2] = 4 - 8 * ip
        return deriv
    
class Lagrange_Segment_FE(Lagrange_FE, FE_1D):
    """
    This class represents a Lagrange finite element on [0,1].
    """
    def __init__(self, order, nodes=None):
        super().__init__()
        self.order = order
        self.ndof = order+1
        if nodes is not None:
            if len(nodes) != self.ndof:
                raise Exception("Invalid number of nodes")
            self.nodes = nodes
        else:
            self.nodes = [ np.array(x) for x in np.linspace(0, 1, self.ndof) ]
        self.barycentric_weights = np.ones(self.ndof)
        for i in range(self.ndof):
            for j in range(self.ndof):
                if i != j:
                    self.barycentric_weights[i] /= (self.nodes[i] - self.nodes[j])   
                    

    def _evaluate_id(self, ip):
        """
        Evaluates the Lagrange segment finite element at the given integration point.

        Uses the barycentric form of the Lagrange polynomials, 
        see https://en.wikipedia.org/wiki/Lagrange_polynomial#Barycentric_form

        l_j(x) = prod_{i!=j} (x-x_i)/(x_j-x_i) 
               = w_j * prod_{i!=j} (x-x_i) with w_j = prod_i (1/(x_j-x_i))
               = w_j / (x-x_j) * l(x) with l(x) = prod_i (x-x_i)
        With further
             1 = sum_i l_i(x) = sum_i (w_i / (x-x_i)) * l(x) 
               = l(x) * sum_i (w_i / (x-x_i))
        we have
        l_j(x) = w_j / (x-x_j) * sum_i (w_i / (x-x_i))
        where the last sum is a does not depend on j.

        Evaluation costs are hence O(ndof) instead of O(ndof^2) for the naive approach.

        Parameters:
        ip (numpy.ndarray): The integration point at which to evaluate the finite element.

        Returns:
        numpy.ndarray: The values of the Lagrange segment finite element at the given integration point.
        """

        d = np.linspace(0, 1, self.order+1)
        l = np.ones([self.order+1])
        for i in range (0, self.order+1):
            for j in range (0, self.order+1):
                if j != i:
                    l[i] = l[i]*(ip[0]-d[j])/(d[i]-d[j])
        
        return l

        #if ip[0] in self.nodes:
        #    ret = np.zeros(self.ndof)
        #    ret[self.nodes.index(ip[0])] = 1
        #else:
        #    denom = sum([self.barycentric_weights[i]/(ip[0]-self.nodes[i]) for i in range(self.ndof)])
        #    ret = self.barycentric_weights.copy()
        #    for i in range(self.ndof):
        #        ret[i] /= (ip[0]-self.nodes[i]) * denom
        #return ret


    def __str__(self):
        return f"Lagrange Segment Finite Element(order={self.order})\n" + super().__str__()
    
#class Legendre_Segment_FE(Lagrange_FE, FE_1D):

from methodsnm.recpol import *
class RecPol_Segment_FE(FE_1D):
    #hence subclass of FE_1D"
    """
    This class represents a Recursive Polynomial finite element on [0,1].
    """
    def __init__(self, order, recpol):
        super().__init__()
    #additional parameter on top of super attributes
        self.order = order
        self.ndof = order+1
        self.recpol = recpol
    #assign the recursive polynomial "recpol"

    def _evaluate_id(self, ip):
        return self.recpol.evaluate_all(2*ip-1, self.order)
    #evaluation of polynomial at one point id using evaluate_all(x,n) of recpol.py"
    #affine map from [-1:1] to [0,1] given by 2*x-1 ????

    def _evaluate_id_array(self, ip):
        return self.recpol.evaluate_all(2*ip[:,0]-1, self.order)
    # here evaluation at at many points in array

    def __str__(self):
        return f"RecPol Segment Finite Element(recpol={self.recpol}, order={self.order})\n" + super().__str__()

def Legendre_Segment_FE(order):
    return RecPol_Segment_FE(order, LegendrePolynomials())
#Note by def of RecPol_Segment we need to parameters: order and the recursive polynomial, function returns the class
#with its functions
# here the legendre polynom as recursive polynomial, 

#need to carry paramter to insert them in JacobiPolynomials in RecPol_Segment_FE to 
def Jacobi_Segment_FE(order, alpha, beta):
    return RecPol_Segment_FE(order, JacobiPolynomials(alpha, beta))


class IntegratedLegendre_Segment_FE(FE_1D):
    """
    This class represents a finite element on [0,1]
    that combines the lowest order P1 element with
    integrated Legendre polynomials of higher order.
    """
    def __init__(self, order):
        super().__init__()
        self.order = order
        self.ndof = order+1
        self.recpol = IntegratedLegendrePolynomials()

    def _evaluate_id(self, ip):
        ret = np.empty(self.ndof)
        ret[0:2] = np.array([1-ip[0],ip[0]])
        ret[2::] = self.recpol.evaluate_all(2*ip-1, self.order-1)[0,1::]
        return ret

    def _evaluate_id_array(self, ip):
        ret = np.empty((len(ip),self.ndof))
        ret[:,0] = 1-ip[:,0]
        ret[:,1] = ip[:,0]
        ret[:,2::] = self.recpol.evaluate_all(2*ip[:,0]-1, self.order-1)[:,1::]
        return ret

    def __str__(self):
        return f"RecPol Segment Finite Element(recpol={self.recpol}, order={self.order})\n" + super().__str__()
