from abc import ABC, abstractmethod
import numpy as np
from numpy import array, einsum
from methodsnm.intrule import *
from methodsnm.fe import *
from numpy.linalg import det, inv
from methodsnm.meshfct import ConstantFunction

class FormIntegral(ABC):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

class LinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_vector(self, fe, trafo):
        raise NotImplementedError("Not implemented")

class SourceIntegral(LinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff #here coeff is global function (hence containg a mesh, a function and _evaluating(ip,trafo) method f(trafo(ip))

    def compute_element_vector(self, fe, trafo, intrule = None):
        if intrule is None:
            intrule = select_integration_rule(2*fe.order, fe.eltype)
        
        #intrule returns e.g. MidPointRule, hence self.points, self.weight, self.exactness 
        quad_p = intrule.nodes
        n_q = len(quad_p)
        quad_w = intrule.weights
        
        shapes_fe = fe.evaluate(quad_p)   #values of finite element basis at quadrature points gives (n_q x ndof) matrix
        coeffs = self.coeff.evaluate(quad_p, trafo) #mesh function has evaluate (for points and arrays) coeffs[k]=f(trafo(quad_g[k])
        
        jac_ev= trafo._jacobian_array(quad_p)
        cor = np.array([np.absolute(np.linalg.det(jac_ev[i,:,:])) for i in range(n_q)])
        
        f = np.zeros(fe.ndof)
        for i in range(fe.ndof):
            for k in range(n_q):
                f[i] = f[i] + coeffs[k]*quad_w[k]*cor[k]*shapes_fe[k,i]
        return f
        
       # need f(trafo(quad_p[])) for sources integral, where source function ???

class SourceIntegral_Lehrenfeld(LinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff #here coeff is global function (hence containg a mesh, a function and _evaluating(ip,trafo) method f(trafo(ip))

    def compute_element_vector(self, fe, trafo, intrule = None):
        if intrule is None:
            intrule = select_integration_rule(2*fe.order, fe.eltype)    
        shapes = fe.evaluate(intrule.nodes)
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        weights = [w*c*abs(det(trafo.jacobian(ip))) for ip,w,c in zip(intrule.nodes,intrule.weights,coeffs)]
        return np.dot(shapes.T, weights)

class BilinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_matrix(self, fe, trafo):
        raise NotImplementedError("Not implemented")

class MassIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        #a = intrule #returns e.g. MidPointRule, hence self.points, self.weight, self.exactness 
        quad_p = intrule.nodes
        n_q = len(quad_p)
        quad_w = intrule.weights
        
        shapes_test = fe_test.evaluate(quad_p)
        shapes_trial = fe_test.evaluate(quad_p)       # here trivial case: "trial is test"
        coeffs = self.coeff.evaluate(quad_p, trafo)
        
        jac_ev= trafo._jacobian_array(quad_p)
        #jac_ev = np.array([trafo.jacobian(quad_p[i]) for i in range(n_q)])
        cor = np.array([np.absolute(np.linalg.det(jac_ev[i,:,:])) for i in range(n_q)])
        
        m = np.zeros((fe_test.ndof, fe_test.ndof))
        for i in range(fe_test.ndof): 
            for j in range(fe_trial.ndof): 
                for k in range(n_q): 
                    m[i,j] = m[i,j] + quad_w[k]*cor[k]*shapes_trial[k,j]*shapes_test[k,i]
                    
        return m
        #raise NotImplementedError("Not implemented")

class MassIntegral_Lehrenfeld(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)  

        shapes_test = fe_test.evaluate(intrule.nodes)
        shapes_trial = fe_test.evaluate(intrule.nodes)
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        F = trafo.jacobian(intrule.nodes)
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        ret = einsum("ij,ik,i,i,i->jk", shapes_test, shapes_trial, adetF, coeffs, intrule.weights)
        return ret


class LaplaceIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        #a = intrule #returns e.g. MidPointRule, hence self.points, self.weight, self.exactness 
        quad_p = intrule.nodes
        n_q = len(quad_p)
        quad_w = intrule.weights
        
        shapes_test = fe_test._evaluate_deriv(quad_p)        #ableitungen !!!
        shapes_trial = fe_test._evaluate_deriv(quad_p)       # here trivial case: "trial is test"
        coeffs = self.coeff.evaluate(quad_p, trafo)
        
        deri_ev= trafo._jacobian_array(quad_p)  #funct das
        cor = np.array([np.absolute(np.linalg.det(deri_ev[i,:,:])) for i in range(n_q)])
        
        lap = np.zeros((fe_test.ndof, fe_test.ndof))
        for i in range(fe_test.ndof): 
            for j in range(fe_trial.ndof): 
                for k in range(n_q): 
                    lap[i,j] = lap[i,j] + quad_w[k]*cor[k]*shapes_trial[k,j]*shapes_test[k,i]*1/(deri_ev[k]*deri_ev[k])
        #innere ableitung problem 1/trafo'(traf(a)) brauchen aber trafo^{-1}(a)
                    
        return m
        #raise NotImplementedError("Not implemented")

class LaplaceIntegral_Lehrenfeld(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        dshapes_ref_test = fe_test.evaluate(intrule.nodes, deriv=True)
        dshapes_ref_trial = fe_test.evaluate(intrule.nodes, deriv=True)
        F = trafo.jacobian(intrule.nodes)
        invF = array([inv(F[i,:,:]) for i in range(F.shape[0])])
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        ret = einsum("ijk,imn,ijl,iml,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, invF, invF, adetF, coeffs, intrule.weights)
        return ret

class ConvectionIntegral(BilinearFormIntegral):
    def __init__(self, coeff=ConstantFunction(1), beta=array([1, 1])):
        self.coeff = coeff
        self.beta = beta

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule=None):
        if intrule == None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception('Finite elements must have the same el. type')
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)

        jacobian = trafo.jacobian(intrule.nodes)
        determinant = np.linalg.det(jacobian)
        shapes_test = fe_test.evaluate(intrule.nodes)
        shapes_trial = np.array([np.linalg.inv(jacobian)[k].T@fe_trial.evaluate(intrule.nodes[k], deriv=True) for k in range(len(intrule.nodes))])

        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        n_quad = len(intrule.nodes)
        convection_matrix = np.zeros((fe_test.ndof, fe_trial.ndof))
        for i in range(fe_test.ndof):
            for j in range(fe_trial.ndof):
                for k in range(n_quad):
                    convection_matrix[i][j] += coeffs[k] * intrule.weights[k] * abs(determinant[i]) * \
                                            np.dot(self.beta, shapes_trial[k][:, j]) * shapes_test[k][i]
        return convection_matrix

            
        #coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        #ret = einsum("ijk,imn,ijl,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, invF, adetF, coeffs, intrule.weights)
        #return np.dot(self.beta, ret)
    
    
#################################################################################################################################################################
class MassIntegral_Dirichlet(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        #a = intrule #returns e.g. MidPointRule, hence self.points, self.weight, self.exactness 
        quad_p = intrule.nodes
        n_q = len(quad_p)
        quad_w = intrule.weights
        
        shapes_test = fe_test.evaluate(quad_p)
        shapes_trial = fe_test.evaluate(quad_p)       # here trivial case: "trial is test"
        coeffs = self.coeff.evaluate(quad_p, trafo)
        
        jac_ev= trafo._jacobian_array(quad_p)
        #jac_ev = np.array([trafo.jacobian(quad_p[i]) for i in range(n_q)])
        cor = np.array([np.absolute(np.linalg.det(jac_ev[i,:,:])) for i in range(n_q)])
        
        m = np.zeros((fe_test.ndof, fe_test.ndof))
        for i in range(1,fe_test.ndof-1): 
            for j in range(1,fe_trial.ndof-1): 
                for k in range(n_q): 
                    m[i,j] = m[i,j] + quad_w[k]*cor[k]*shapes_trial[k,j]*shapes_test[k,i]
        m[0,0]=1
        m[fe_test.ndof-1,fe_test.ndof-1]=1
                    
        return m
    
class SourceIntegral_Dirichlet(LinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff #here coeff is global function (hence containg a mesh, a function and _evaluating(ip,trafo) method f(trafo(ip))

    def compute_element_vector(self, fe, trafo, intrule = None):
        if intrule is None:
            intrule = select_integration_rule(2*fe.order, fe.eltype)
        
        #intrule returns e.g. MidPointRule, hence self.points, self.weight, self.exactness 
        quad_p = intrule.nodes
        n_q = len(quad_p)
        quad_w = intrule.weights
        
        shapes_fe = fe.evaluate(quad_p)   #values of finite element basis at quadrature points gives (n_q x ndof) matrix
        coeffs = self.coeff.evaluate(quad_p, trafo) #mesh function has evaluate (for points and arrays) coeffs[k]=f(trafo(quad_g[k])
        
        jac_ev= trafo._jacobian_array(quad_p)
        cor = np.array([np.absolute(np.linalg.det(jac_ev[i,:,:])) for i in range(n_q)])
        
        f = np.zeros(fe.ndof)
        for i in range(1,fe.ndof-1):
            for k in range(n_q):
                f[i] = f[i] + coeffs[k]*quad_w[k]*cor[k]*shapes_fe[k,i]
        return f
    
class LaplaceIntegral_Dirichlet(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule = None):
        if intrule is None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception("Finite elements must have the same el. type")
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        
        dshapes_ref_test = fe_test.evaluate(intrule.nodes, deriv=True)
        dshapes_ref_trial = fe_test.evaluate(intrule.nodes, deriv=True)
        F = trafo.jacobian(intrule.nodes)
        invF = array([inv(F[i,:,:]) for i in range(F.shape[0])])
        adetF = array([abs(det(F[i,:,:])) for i in range(F.shape[0])])
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        ret = einsum("ijk,imn,ijl,iml,i,i,i->kn", dshapes_ref_test, dshapes_ref_trial, invF, invF, adetF, coeffs, intrule.weights)
        for i in range (len(dshapes_ref_test)):
            ret[i:len(dshapes_ref_test)-1]=0
            ret[i:0]=0
            ret[0:i]=0
            ret[len(dshapes_ref_test)-1:i]=0
        ret[0:0]=1
        ret[len(dshapes_ref_test)-1:len(dshapes_ref_test)-1]=1
        return ret
    
def error_calc(u_h, u_exact, intorder=5, intrule = None):
    
    fes = u_h.fes
    mesh = fes.mesh
    sumint = 0
    
    for elnr in range (len(mesh.elements())):
            trafo = mesh.trafo(elnr)
            fe = u_h.fes.finite_element(elnr)
            if intrule is None:
                intrule = select_integration_rule(2*fe.order, fe.eltype)
            #############################
            quad_p = intrule.nodes
            n_p = len(quad_p)
            quad_w = intrule.weights
            #############################
            u_hval = u_h.evaluate(quad_p, trafo)
            u_exactval = u_exact.evaluate(quad_p, trafo)
            ############################
            diff = np.zeros(n_p)
            diff = (u_hval-u_exactval)**2
            
            F = trafo.jacobian(quad_p)
            w = array([np.absolute(np.linalg.det(F[i,:,:])) * intrule.weights[i] for i in range (F.shape[0])])
            sumint += np.dot(w, diff)
            
    return np.sqrt(sumint) 