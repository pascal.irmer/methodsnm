from abc import ABC, abstractmethod
import numpy as np
from numpy import array

from methodsnm.mesh import Mesh

from methodsnm.trafo import IntervalTransformation

class Mesh1D(Mesh):
    def __init__(self, points, sub_intervals=1):
        self.dimension = 1
        ne = (len(points)-1) * sub_intervals
        nv = ne + 1
        self.points = np.empty(nv)
        #anzahl der vertices, brauchen plus eins weil rechter Randpunkt dazu muss
        nme = len(points)-1
        for i in range(nme):
            for j in range(sub_intervals):
                self.points[i*sub_intervals+j] = points[i] + j*(points[i+1]-points[i])/sub_intervals
        #nehmen den linken wert und addieren die unterteilungen drauf, deshalb nme eins kleiner, da rechter Punkt bleibt
        self.points[-1] = points[-1]
        #deshalb setze den letzten punkt eins zu eins in self.points ein
        self.vertices = np.arange(nv)
        #erzzugt array der vertices mit integern betitelt
        self.edges = np.array([[i,i+1] for i in range(ne)])
        #arrays zwischen den vertices, welche mit integern nummeriert sind (deswegen nur ne und nicht nv=ne+1)
        self.bndry_vertices = np.array([0,nv-1])
        #
        self.bndry_edges = None

    def trafo(self, elnr, codim=0, bndry=False):
        if codim > 0 or bndry:
            raise NotImplementedError("Not implemented")
        return IntervalTransformation(self, elnr)

    def uniform_refine(self):
        return Mesh1D(self.points, sub_intervals=2)
    #nimmt nimmt mesh und unterteil es nochmal (deshalb sub_intervals=2 default) macht die Unterteilung feiner
