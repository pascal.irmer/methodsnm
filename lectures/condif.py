from methodsnm.mesh_2d import *
from methodsnm.visualize import *
from numpy import pi, cos, sin, arctan, tanh, tan
from methodsnm.forms import *
from methodsnm.formint import *
from scipy.sparse.linalg import spsolve
from methodsnm.fes import *

def NumericalStudies(N,mesh,beta,alpha,gamma):
    self.N = N
    self.mesh = mesh
    h =1/self.N
    fes = P3_Triangle_Space(mesh)
    
    blf = BilinearForm(fes)
    c = GlobalFunction(lambda x: alpha, mesh = mesh_mod)
    blf += LaplaceIntegral_Lehrenfeld(c)
    blf += ConvectionIntegral(beta=beta)
    blf.assemble()

    lf = LinearForm(fes)

    conv_diff = lambda x, alpha, beta, gamma: \
        beta[0]*(x[1] + (np.e**(beta[1]*x[1]/alpha) - 1)/
             (1 - np.e**(beta[1]/alpha))) + \
        beta[1]*(x[0] + (np.e**(beta[0]*x[0]/alpha) - 1) /
             (1 - np.e**(beta[0]/alpha)))
    f = GlobalFunction(lambda x: conv_diff(x, alpha, beta, gamma), mesh = mesh_mod)
    lf += SourceIntegral(f)
    lf.assemble()
    
    dirichlet_mask = np.zeros(fes.ndof,dtype=bool)
    for belnr, verts in enumerate(mesh.elements(bndry=True)):
        dirichlet_mask[fes.element_dofs(belnr, bndry=True)]=True
    non_dirichlet_mask = np.logical_not(dirichlet_mask)
    
    A = blf.matrix[non_dirichlet_mask,:][:,non_dirichlet_mask]
    b = lf.vector[non_dirichlet_mask]
    uh = FEFunction(fes)
    uh.vector[non_dirichlet_mask] = spsolve(A, b)
    
    error = error_calc(uh, u_exact)
    beta_max = beta.flat[np.abs(beta).argmax()]
    Pec = beta_max /(alpha*h)
    return np.array([error,h, Pec])